﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge_Auto_Dagwaarde
{
    enum Brandstof
    {
        Benzine = 100,
        LPG = 150,
        Diesel = 90,
        Elektrisch = 130
    };
}
