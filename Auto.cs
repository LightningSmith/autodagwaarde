﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge_Auto_Dagwaarde
{
    class Auto
    {
        private int kmstand;
        private List<string> kenteken = new List<string>();
        public string[] auto = { "Auto 1", "Auto 2", "Auto 3", "Auto 4" };
        private int autoDagwaarde;
        //private int formule = (500000 / kmStand) * brandstofWaarde;

        public Auto()
        {

        }

        public Auto(string brandstof, int brandstofValue)
        {
            Random random = new Random();
            int brandstofSoort = random.Next(0, 4);

            if(brandstofSoort == 0)
            {
                brandstof = Brandstof.Benzine.ToString;
                brandstofValue = (int)Brandstof.Benzine;
            }
            else if(brandstofSoort == 1)
            {
                brandstof = Brandstof.Diesel.ToString;
                brandstofValue = (int)Brandstof.Diesel;
            }
            else if(brandstofSoort == 2)
            {
                brandstof = Brandstof.Elektrisch.ToString;
                brandstofValue = (int)Brandstof.Elektrisch;
            }
            else if(brandstofSoort == 3)
            {
                brandstof = Brandstof.LPG.ToString;
                brandstofValue = (int)Brandstof.LPG;
            }
        }

        public int KmStand
        {
            get
            {
                return kmstand;
            }
            set
            {
                if (kmstand <= 1)
                {
                    kmstand = 1;
                }

                Random random = new Random();
                int aantalkm = random.Next(1000, 20000);

                int km = RijdKilometers(aantalkm);
                
                if(km > 0)
                {
                    kmstand += km;
                }else if(kmstand >= 100000)
                {
                    
                }
            }
        }

        public List<string> Kenteken
        {
            get
            {
                return kenteken;
            }
            set
            {
                kenteken.Add("71-XKX-2");
                kenteken.Add("5-XVN-10");
                kenteken.Add("95-FS-PN");
                kenteken.Add("4-TTV-68");
            }
        }

        public Auto(string Kenteken)
        {
            for(int i = 0; i < kenteken.Count; i++)
            {
                Kenteken = kenteken[i];
            }
        }

        public int RijdKilometers(int aantalKm)
        {
            if(aantalKm > 0)
            {
                return aantalKm;
            }

            return 0;
        }

        public int AutoDagwaarde
        {
            get
            {
                return autoDagwaarde;
            }

            set
            {

            }
        }
    }
}
